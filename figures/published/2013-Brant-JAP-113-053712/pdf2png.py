#!/usr/bin/python3

# recursively find all pdfs in the current directory and convert them to high
# resolution pngs

from os import system
from glob import glob

# globber the current dir to find all the pdfs
input_files = glob("**/*.pdf", recursive=True)
output_files = [i.replace(".pdf", ".png") for i in input_files]

# pixel density
dpi = 750
quality = 100

# convert each file using a system call
for ifile, ofile in zip(input_files, output_files):
    # use ImageMagick to do the conversion
    cmd = "convert -density %d -quality %d %s %s" % (dpi, quality, ifile, ofile)
    print(cmd)
    system(cmd)
